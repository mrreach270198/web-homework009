import { Container } from "react-bootstrap";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import ProtectedRoute from "./auth/ProtectedRoute";
import Detail from "./components/Detail";
import Account from "./view/Account";
import Auth from "./view/Auth";
import Home from "./view/Home";
import MyNav from "./view/MyNav";
import Video from "./view/Video";
import Welcome from "./view/Welcome";

function App() {
  return (
    <div>
      <Container>
      <BrowserRouter>
        <MyNav />
        <Switch>
            <Route exact path="/"><Home/></Route>
            <Route path="/video"><Video/></Route>
            <Route path="/account" component={Account} />
            <ProtectedRoute path="/welcome"><Welcome/> </ProtectedRoute>
            <Route path="/auth" component={Auth} />
            <Route path="/detail/:id" component={Detail}/>
        </Switch>
      </BrowserRouter>
      </Container>
    </div>
  );
}

export default App;
