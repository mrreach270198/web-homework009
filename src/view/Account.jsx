import React from "react";
import { NavLink, Switch, Route, BrowserRouter } from "react-router-dom";
import DisplayAccount from "../components/DisplayAccount";

function Account() {
  return (
    <BrowserRouter>  
    <div>
      <h1>Account</h1>
      <ul>
        <li>
          <NavLink to="/netflix">Netflix</NavLink>
        </li>
        <li>
          <NavLink to="/zillow-group">Zillow Group</NavLink>
        </li>
        <li>
          <NavLink to="/yahoo">Yahoo</NavLink>
        </li>
        <li>
          <NavLink to="/modus-create">Modus Create</NavLink>
        </li>
      </ul>
        <Switch>
          <Route path="/:id" children={<DisplayAccount/>} />
        </Switch>
    </div>
    </BrowserRouter>
  );
}

export default Account;
