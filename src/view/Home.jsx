import React, { useState } from 'react'
import HomeCompo from '../components/HomeCompo'

function Home() {
    const [data, setData] = useState([
        {
            id : 1,
            title : 'Java',
            image : 'https://www.indiahires.in/wp-content/uploads/2020/07/Java-Developer-Interview.jpg',
            content : 'I love Java'
        },
        {
            id : 2,
            title : 'Spring',
            image : 'https://www.systembug.me/assets/img/spring.png',
            content : 'I love Spring'
        },
        {
            id : 3,
            title : 'React JS',
            image : 'https://reactjs.org/logo-og.png',
            content : 'I love React JS'
        },
        {
            id : 4,
            title : 'Javascript',
            image : 'https://miro.medium.com/max/2732/1*BPSx-c--z6r7tY29L19ukQ.png',
            content : 'I love Javascript'
        },
        {
            id : 5,
            title : 'CSS',
            image : 'https://res.cloudinary.com/practicaldev/image/fetch/s--nt5Z8IRB--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://thepracticaldev.s3.amazonaws.com/i/q3xa7ii9iei1b8kvs451.jpg',
            content : 'I love CSS'
        },
        {
            id : 6,
            title : 'Docker',
            image : 'https://res.cloudinary.com/practicaldev/image/fetch/s--o19W-00j--/c_imagga_scale,f_auto,fl_progressive,h_900,q_auto,w_1600/https://sinpapel.es/labinabox/img/dockerrescue.png',
            content : 'I love Docker'
        }
    ])
    return (
        <div>
            <HomeCompo items={data}/>
        </div>
    )
}

export default Home
