import { Button } from 'react-bootstrap'
import React from 'react'
import Auth from '../auth/Auth'
import { useHistory } from 'react-router'

function Welcome() {
    let history = useHistory();
    return (
        <div>
            <h1>Welcome</h1>
            <Button variant="primary" type="button"
                onClick={()=>
                    Auth.logout(()=>{
                        history.push('/auth')
                    })
                }>
                Logout
            </Button>
        </div>
    )
}

export default Welcome
