import React from "react";
import VideoCompo from "../components/VideoCompo";
import { BrowserRouter, Route, Switch, useRouteMatch } from "react-router-dom";
import Movie from "../components/Movie";
import Animation from "../components/Animation";

function Video() {
    let {url } = useRouteMatch();
  return (
    <div>
      <h1>Video</h1>
      <BrowserRouter>
        <VideoCompo path={url}/>
        <Switch>
        <Route path={`${url}/movie`}><Movie/></Route>
        <Route path={`${url}/animation`}><Animation/></Route>
        </Switch>
    </BrowserRouter>
    </div>
  );
}

export default Video;
