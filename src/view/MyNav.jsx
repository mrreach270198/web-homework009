import React from "react";
import { Navbar, Nav, FormControl, Button, Form } from "react-bootstrap";
import { NavLink } from "react-router-dom";

function MyNav() {
  return (
    <div>
      <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">React-Router Nou Reach</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link as={NavLink} to='/'>Home</Nav.Link>
            <Nav.Link as={NavLink} to='/video'>Video</Nav.Link>
            <Nav.Link as={NavLink} to='/account'>Account</Nav.Link>
            <Nav.Link as={NavLink} to='/welcome'>Welcome</Nav.Link>
            <Nav.Link as={NavLink} to='/auth'>Auth</Nav.Link>
          </Nav>
          <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}

export default MyNav;
