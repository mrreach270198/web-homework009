import React from 'react'
import { Redirect, Route } from 'react-router'
import Auth from './Auth'

function ProtectedRoute({children}) {
    return (
        <div>
            <Route render={()=> Auth.isAuth ? children : <Redirect to="/auth"/>}/>
        </div>
    )
}

export default ProtectedRoute
