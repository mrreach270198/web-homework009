class Auth{

    constructor(){
        this.isAuth = false
    }

    login(li){
        this.isAuth = true
        li()
    }

    logout(lo){
        this.isAuth = false
        lo()
    }
}
export default new Auth()
