import React from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { BrowserRouter, NavLink, Route, Switch, useHistory} from "react-router-dom";
import Detail from "./Detail";

function MyCard({ items }) {

  let history = useHistory();

  return (
    <div>
      <BrowserRouter>
      <Row>
        {items.map((item, index) => (
          <Col md="3" className="my-2" key={index}>
            <Card>
              <Card.Img variant="top" src={item.image} />
              <Card.Body>
                <Card.Title>{item.title}</Card.Title>
                <Card.Text>
                  {item.content}
                </Card.Text>
                <Button as={NavLink} to={`/detail/${item.id}`} onClick={()=> history.push(`/detail/${item.id}`)} variant="info">Read</Button>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>
      <Switch>
        <Route path='/detail/:id'>
          <Detail/>
        </Route>
      </Switch>
      </BrowserRouter>
    </div>
  );
}

export default MyCard;
