import React from 'react'
import { ButtonGroup, Button } from 'react-bootstrap'
import { NavLink} from "react-router-dom";

function VideoCompo({path}) {
    return (
        <div>
            <ButtonGroup aria-label="Basic example">
            <Button as={NavLink} to={`${path}/movie`} variant="secondary">Movie</Button>
            <Button as={NavLink} to={`${path}/animation`} variant="secondary">Animation</Button>
            </ButtonGroup>
        </div>
    )
}

export default VideoCompo
