import React from "react";
import { ButtonGroup, Button } from "react-bootstrap";
import {useRouteMatch, NavLink,} from "react-router-dom";
import DisplayMovie from "./DisplayMovie";
import useQuery from './useQuery';

function Animation() {

  let { url } = useRouteMatch();
  let query = useQuery();

  return (
    <div>
      <h1>Animation Category</h1>
      <ButtonGroup aria-label="Basic example">
        <Button as={NavLink} to={`${url}?type=Action`} variant="secondary">
          Action
        </Button>
        <Button as={NavLink} to={`${url}?type=Romance`} variant="secondary">
          Romance
        </Button>
        <Button as={NavLink} to={`${url}?type=Comedy`} variant="secondary">
          Comedy
        </Button>
      </ButtonGroup>

      <DisplayMovie type={query.get("type")} />
    </div>
  );
}

export default Animation;
